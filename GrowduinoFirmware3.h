#pragma once

#include "Arduino.h"
#include <math.h>

#define MINVALUE -INFINITY


#define OUTPUTS 12
#define RELAY_START 34

#define LOGGERS 12

#define ANALOG_READ_AVG_TIMES 30
#define ANALOG_READ_AVG_DELAY 10
#define LIGHT_SENSOR_PIN_1 8
#define LIGHT_SENSOR_PIN_2 9


#define DHT22_PIN 22
#define ONEWIRE_PIN 23
#define ONEWIRE_PIN2 24
#define USOUND_TRG 25
#define USOUND_ECHO 26
#define SENSOR_SDA 29  // Y
#define SENSOR_SCL 30  // B

//i2c pH sensor
#define I2C_MAX11647adr  0x36

#define USE_EC_SENSOR 1
#define EC_ENABLE 27
#define EC_DATA 28
#define EC_SAMPLE_TIMES 100
#define EC_CUTOFF 550
#define EC_TIMEOUT 10000

// when reading from ADC, values read higher than this are considered as error (CO2 sensor)
#define ADC_CUTOFF 1020
#define CO2_DATA 12

//LCD

#define LCD_BUFFER_LINES 12
#define LCD_DISPLAY_LINES 4
#define LCD_LINE_LEN 20

#define TH_SDA 31
#define TH_SCL 32

//#define DEBUG 1
#ifdef DEBUG
#define DEBUG_PH 1
#endif

#include "util.h"
#include "sensors.h"

unsigned long getSeconds();

int daymin(unsigned long seconds);
int daymin();

struct SensorConfig {
  int valuecheck;
  int co2_400;
  int co2_40k;
  int ph_4;
  int ph_7;
  int ec_low_ion;
  int ec_high_ion;
  int ec_offset;
  bool co2_probed;
  bool co2_digital;
};

