#include "GrowduinoFirmware3.h"

/* all sensors that are not complex enough to have its own file */

#include <dht.h>
#include <math.h>

#include <SoftwareWire.h>
#include <MHZ19.h>

dht DHT;

bool dht_failure = true;
#include <SoftwareSerial.h>

// Not used, but needed by Adafruit_BME280.h
#include <SPI.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

extern SensorConfig sensor_config;

SoftwareWire sensor_wire(SENSOR_SDA, SENSOR_SCL);
SoftwareWire th_soft_wire(TH_SDA, TH_SCL);

MHZ19 mhz(&Serial1);

bool th_status = false;

union ByteInt {
  int val;
  struct {
    // Access to the bytes of 'val'
    byte lo, hi;
  } bytes;
};

Adafruit_BME280 th_sensor;

float get_TH_temperature() {
  if (th_status) {
    float temp = th_sensor.readTemperature();
    if ((temp < -140) || (temp > 140) || isnan(temp)) {
      th_status = false;
      return MINVALUE;
    }
    return 10 * th_sensor.readTemperature();
  } else {
    return MINVALUE;
  }
}


float getDhtTemperature() {
  if (dht_failure) {
    return MINVALUE;
  }
  return DHT.temperature * 10;
}

float get_TH_humidity() {
  if (th_status) {
    return 10 * th_sensor.readHumidity();
  } else {
    return MINVALUE;
  }
}

bool th_init() {
  if (not th_status) {
    th_soft_wire.begin();
    if (!th_status) {
      unsigned status = th_sensor.begin(0x76, &th_soft_wire);
      if (!status) {
        Serial.println("D: Could not find a valid BME280 sensor");
        th_status = false;
      } else {
        th_status = true;
      }
    }
  }
  return th_status;
}

float getDhtHumidity() {
  if (dht_failure) {
    return MINVALUE;
  }
  return DHT.humidity * 10;
}

void dhtMeasure() {
  int chk = DHT.read22(DHT22_PIN);
  dht_failure = true;
  switch (chk) {
    case DHTLIB_OK:
      dht_failure = false;
      break;
    case DHTLIB_ERROR_CHECKSUM:
      Serial.println(F("D: DHT Checksum error"));
      break;
    case DHTLIB_ERROR_TIMEOUT:
      Serial.println(F("D: DHT Time out error"));
      break;
    default:
      Serial.println(F("D: DHT Unknown error"));
      break;
  }
}

void doPhSetup() {
  sensor_wire.beginTransmission(I2C_MAX11647adr);
  sensor_wire.write(0b11010110);
  sensor_wire.write(0b00000010);
  sensor_wire.endTransmission();    // stop transmitting
}

float getPhMeasure() {
  int reading = 0;
  int reading2 = 0;

  sensor_wire.requestFrom(I2C_MAX11647adr, 2);
  if (2 <= sensor_wire.available()) { // if two bytes were received
    reading = sensor_wire.read();  // receive high byte (overwrites previous reading)
    reading2 = sensor_wire.read();
#ifdef DEBUG_PH
    Serial.print("D: pH hi ");
    Serial.println(reading, BIN);   // print the reading
    Serial.print("D: pH lo ");
    Serial.println(reading2, BIN);   // print the reading
#endif
    if ((reading & 0x02) == 0) {
      reading = reading & 0x03;
    }
    reading = reading << 8;    // shift high byte to be high 8 bits
    reading |= reading2;
#ifdef DEBUG_PH
    Serial.print("D: pH final ");
    Serial.println(reading);   // print the reading
#endif
    return (float) reading + 512; // return positive value
  } else {
#ifdef DEBUG_PH
    Serial.println("D: pH MINVALUE");
#endif
    return MINVALUE;
  }
}

float calibrate_ph(float raw_data) {

  float ph_4_val = 4;
  float ph_7_val = 7;
  float ph_4 = (float) sensor_config.ph_4;
  float ph_7 = (float) sensor_config.ph_7;
  if (isinf(raw_data)) {
    return MINVALUE;
  }
  float slope = (ph_7 - ph_4) / (ph_7_val - ph_4_val);
  float pH = ph_4_val + (((float) raw_data - ph_4) / slope);

  return pH;
}


void ec_enable() {
  pinMode(EC_DATA, INPUT);
  pinMode(EC_ENABLE, OUTPUT);
}

float getECMeasure() {
  long lowPulseTime_t = 0;
  long highPulseTime_t = 0;
  long lowPulseTime = 0;
  long highPulseTime = 0;
  float pulseTime;

  digitalWrite(EC_ENABLE, HIGH); // power up the sensor
  delay(100);
  for (unsigned int j = 0; j < EC_SAMPLE_TIMES; j++) {
    highPulseTime_t = pulseIn(EC_DATA, HIGH, EC_TIMEOUT);
    if (j == 0 and highPulseTime_t == 0)  {//abort on timeout on first read
      return MINVALUE;
    }

    highPulseTime += highPulseTime_t;
    if (highPulseTime_t == 0) {
      return MINVALUE;
    }
    lowPulseTime_t = pulseIn(EC_DATA, LOW, EC_TIMEOUT);
    lowPulseTime += lowPulseTime_t;

    if (lowPulseTime_t == 0) {
      return MINVALUE;
    }

  }

  lowPulseTime = lowPulseTime / EC_SAMPLE_TIMES;
  highPulseTime = highPulseTime / EC_SAMPLE_TIMES;

  pulseTime = (lowPulseTime + highPulseTime) / 2;

  digitalWrite(EC_ENABLE, LOW); // power down the sensor

  if (pulseTime >= EC_CUTOFF || pulseTime < 0) {
    return MINVALUE;
  }

  return pulseTime;
}

float calibrate_ec(float pulseTime) {
  if (isinf(pulseTime)) {
    return MINVALUE;
  }
  float ec = MINVALUE;
  float ec_a, ec_b;

  float c_low = 1.278;
  float c_high = 4.523;
  float ec_high_ion = (float) (sensor_config.ec_high_ion + sensor_config.ec_offset);
  float ec_low_ion = (float) (sensor_config.ec_low_ion + sensor_config.ec_offset);

  ec_a =  (c_high - c_low) / (1 / ec_high_ion - 1 / ec_low_ion);
  ec_b = c_low - ec_a / (float) ec_low_ion;

  ec = 100 * (ec_a / (pulseTime + sensor_config.ec_offset) + ec_b);


#ifdef DEBUG_CALIB
  if (ec != MINVALUE) {
    Serial.print(F("EC pulse time: "));
    Serial.println(pulseTime);
  }
#endif
  return ec;
}

float getCO2DigitalMeasure() {
  MHZ19_RESULT response = mhz.retrieveData();
  if (response == MHZ19_RESULT_OK) {
    return (float) mhz.getCO2();
  } else {
    return MINVALUE;
  }
}

float getCO2Measure() {
  float raw_data;
  if (sensor_config.co2_digital) {
    return getCO2DigitalMeasure();
  }
  raw_data = analogReadAvg(CO2_DATA);
  return raw_data;
}

void co2_init() {
  MHZ19_RESULT response = mhz.retrieveData();
  if (response == MHZ19_RESULT_OK) {
    mhz.setAutoCalibration(false);
  }
}


float calibrate_co2(float raw_data) {
  if (sensor_config.co2_digital) {
    return raw_data;
  }
  float voltage;
  //int co2 = MINVALUE;
  // http://www.veetech.org.uk/Prototype_CO2_Monitor.htm
  float v400ppm = (float) sensor_config.co2_400 / 204.6;
  float v40000ppm = (float) sensor_config.co2_40k / 204.6;
  float deltavs = v400ppm - v40000ppm;
  float A = deltavs / (log10(400) - log10(40000));
  float B = log10(400);

  if (raw_data > ADC_CUTOFF) {
    return MINVALUE;
  }
  voltage = raw_data / 204.6;

  float power = ((voltage - v400ppm) / A) + B;
  float co2ppm = pow(10, power) / 10;
  //co2 = (int) co2ppm;

  return co2ppm;
}

bool getCO2DigitalStatus() {
  MHZ19_RESULT response = mhz.retrieveData();
  return (response == MHZ19_RESULT_OK);
}
